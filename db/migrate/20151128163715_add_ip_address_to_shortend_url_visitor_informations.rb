class AddIpAddressToShortendUrlVisitorInformations < ActiveRecord::Migration
  def change
    add_column :shortend_url_visitor_informations, :ip_address, :string
  end
end
