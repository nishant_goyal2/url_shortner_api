class CreateShortendUrlVisitorInformations < ActiveRecord::Migration
  def change
    create_table :shortend_url_visitor_informations do |t|
      t.string :city
      t.string :state
      t.string :country
      t.integer :ip_address_shortend_url_id

      t.timestamps null: false
    end
  end
end
