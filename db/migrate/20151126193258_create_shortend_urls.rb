class CreateShortendUrls < ActiveRecord::Migration
  def change
    create_table :shortend_urls do |t|
      t.string :original_url
      t.string :shortend_url
      t.integer :user_id
      t.integer :visit_count

      t.timestamps null: false
    end
  end
end
