class RenameColumnInShortendUrlVisitorInformations < ActiveRecord::Migration
  def change
  	rename_column :shortend_url_visitor_informations, :ip_address_shortend_url_id, :shortend_url_id
  end
end
