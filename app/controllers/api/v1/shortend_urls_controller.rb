class Api::V1::ShortendUrlsController < Api::V1::BaseController
  before_action :authenticate

  before_action :set_shortend_url, :only => [:show, :destroy]
  def show
    render(json: @shortend_url.present? ? {"shortend_url" => @shortend_url} : {})
  end

  def index

    @shortend_urls = @user.shortend_urls.paginate(:page => params[:page], :per_page => 10)
    render(
      json: {:shortend_urls => @shortend_urls,
       	:page => params[:page]
      }
    )
  end

  def create
  	@shortend_url = ShortendUrl.new(shortend_url_params)
  	if @shortend_url.save
  		render(:json => {status: :created})
  	else
  		render(json: {:shortend_url => @shortend_url.errors, status: :unprocessable_entity})
  	end
  end

  def destroy
  	if @shortend_url.present?
			@shortend_url.destroy
    	render(json: {})
  	else
  		render(json: {status: "invalid shortend_url id"})
  	end
  end

  def url_redirect
  	@shortend_url = ShortendUrl.find_by_slug_and_by_user_id(params[:slug], @user.id)
  	if @shortend_url.present?
  		redirect_to "http://#{@shortend_url.original_url}"
  	else
  		render(json: {status: "invalid url"})
  	end
  end

  def url_geolocate
  	@shortend_url = ShortendUrl.find_by_slug_and_by_user_id(params[:slug], @user.id)
  	if @shortend_url.present?
  		ShortendUrlVisitorInformation.store_visitor_information(request.remote_ip, @shortend_url.id)
  		render(:json =>  {shortend_url_visitor_info: @shortend_url.shortend_url_visitor_informations.last, status: "geolocation entry found"})
  	else
  		render(:json => {status: "invalid url slug"})
  	end
  end

  private
  def shortend_url_params
  	params[:shortend_url] = JSON.parse(params[:shortend_url])
  	params[:shortend_url][:user_id] = @user.id if params[:shortend_url].present?
    params.require(:shortend_url).permit(:original_url, :shortend_url, :user_id) if params[:shortend_url].present?
  end

  def set_shortend_url
  	@shortend_url = ShortendUrl.find_by_id_and_user_id(params[:id], @user.id)
  end

end