class Api::V1::SessionsController < Api::V1::BaseController

	def create
		params[:user] = JSON.parse(params[:user])
    user = User.find_by(email: create_params[:email])
    if user && user.authenticate(create_params[:password])
      render(json: {status: 201, :api_key => user.api_key})
    else
      return render(:json => {status: 401})
    end
  end

  private
  def create_params
    params.require(:user).permit(:email, :password) if params[:user].present?
  end

end
