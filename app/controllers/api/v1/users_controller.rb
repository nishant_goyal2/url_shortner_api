class Api::V1::UsersController < Api::V1::BaseController

  before_action :authenticate, :only => [:update, :destroy]
  def create
    @user = User.new(user_params)
    if @user.save
      render(:json => {status: :created, :api_key => @user.api_key})
    else
      render(json: {:user => @user.errors, status: :unprocessable_entity})
    end
  end

  def update
    if @user.id == params[:id]
      if @user.update_attributes(user_params)
        render(:json => {status: :updated})
      else
        render(json: {:user => @user.errors, status: :unprocessable_entity})
      end
    else
      render(:json => {status: 401})
    end
  end

  def destroy
    if @user.id == params[:id]
      @user.destroy
      render(:json => {})
    else
      render(:json => {status: 401})
    end
  end

  private
  def user_params
    params[:user] = JSON.parse(params[:user])
    params.require(:user).permit(:name, :email, :password, :password_confirmation) if params[:user].present?
  end
end
