class Api::V1::BaseController < ActionController::Base
  include Api::V1::SessionsHelper
  protect_from_forgery with: :null_session
  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  before_action :destroy_session

  def destroy_session
    request.session_options[:skip] = true
  end

  def not_found
    return api_error(status: 404, errors: 'Not found')
  end

  private
  def authenticate
    authenticate_or_request_with_http_token do |token, options|
      @user = User.where(api_key: token).first
    end
    unless @user
    	head status: :unauthorized
    	return false
  	end
  end
end