class ShortendUrl < ActiveRecord::Base
  belongs_to :user

  BASE_URL = "localhost:3000"

  has_many :shortend_url_visitor_informations, :dependent => :destroy
  validates_presence_of :original_url, :shortend_url, :user_id

  before_validation :generate_shortend_url

  def generate_shortend_url
		self.slug = generate_slug
		self.shortend_url = display_shortend_url
  end

  def display_shortend_url
  	BASE_URL + '/api/v1/sl/' + self.slug
  end

  def generate_slug
  	slug = nil
	  loop do
	    slug = SecureRandom.hex(2)
	    break slug unless ShortendUrl.exists?(slug: slug)
	  end
	  return slug
	end
end
