class User < ActiveRecord::Base
	has_secure_password
	before_create :save_api_key

	validates :name, presence: true, length: { maximum: 250 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
 	VALID_PASSWORD_REGEX = /\A(?=.*[a-zA-Z])(?=.*[0-9]).{6,}\z/i
  validates :email, :presence => true, :format => { :with => VALID_EMAIL_REGEX }, :uniqueness => true
  validates :password, :presence => true, :length => { :minimum => 6 }, :format => { :with => VALID_PASSWORD_REGEX, :message => "should be atleast 6 letters long and alphanumeric." }  
  validates :password_confirmation, presence: true, length: { minimum: 6 }
    
	has_many :shortend_urls, :dependent => :destroy
  def save_api_key
  	self.api_key = generate_api_key
  end

	def generate_api_key
	  token = nil
	  loop do
	    token = SecureRandom.base64.tr('+/=', 'Qrt')
	    break token unless User.exists?(api_key: token)
	  end
	  return token
	end  
end
