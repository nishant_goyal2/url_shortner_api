class ShortendUrlVisitorInformation < ActiveRecord::Base
	require 'net/http'
	require 'uri'
	require 'open-uri'

	belongs_to :shortend_url

	validates_presence_of :ip_address, :shortend_url_id

	def self.store_visitor_information(visitor_ip_address, shortend_url_id)
		free_geo_ip_response = open_link(visitor_ip_address)
		if free_geo_ip_response.present?
			begin
				@free_geo_ip_response_hash = JSON.parse(free_geo_ip_response)	
			rescue
				@free_geo_ip_response_hash = Hash.new if @free_geo_ip_response_hash.nil?		
			end
			ShortendUrlVisitorInformation.create(:ip_address => visitor_ip_address, :country => @free_geo_ip_response_hash["country_name"], :city => @free_geo_ip_response_hash["city"], :state => @free_geo_ip_response_hash["region_name"], :shortend_url_id => shortend_url_id)
		end
	end

	def self.open_link(ip_address)
    retry_attempt = 0
    begin
      @opened_link = Net::HTTP.get(URI.parse("http://freegeoip.net/json/#{ip_address}"))
    rescue Exception => e
      retry_attempt = retry_attempt +1
      if retry_attempt <=3
        sleep 1
        retry
      else
        p "Errors in opening the given link: #{e.message}"
        Rails.logger.error "Errors in opening the given link: #{e.message}"
      end
    end

    return retry_attempt > 3 ? nil : @opened_link
  end
end
