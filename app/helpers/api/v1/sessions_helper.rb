module Api::V1::SessionsHelper

	#------------------------User Signin--------------------------------------------
  def user_sign_in(user)
    response.header["api_key"]="Token token=#{user.api_key}"      
    self.current_user = user
    return user
  end
  #-----------------------Current Signed in User setter method---------------------------------------
  def current_user=(user)
    current_user=user
  end

  #-----------------------Current Signed in User getter method---------------------------------------
  def current_user
    current_user ||= User.find_by_remember_token(cookies[:user_remember_token]) if cookies[:user_remember_token].present?
  end
end
